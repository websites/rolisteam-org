# Contributors

We would like to express our gratitude to all the contributors who have helped advance this project. Your help, ideas, and code have been invaluable.

## List of Contributors

### Lead Developers

- **Renaud Guezzenec** - Founder and lead developer
- **Ben Cooksley** - Developer
- **KDE** - Web hoster

### Notable Contributions

- **Yann Escarbassiere**
  - Traduction
  - Testing and debugging

### Other Contributors

We also thank all the contributors who have submitted pull requests, reported bugs, or provided suggestions to improve the project:

- **Carl Schwan**
- **Rolisteam discord server**

## How to Contribute

If you would like to see your name here, feel free to contribute to the project! Check the [contributing guide](https://doc.rolisteam.org/en/coding.html) to get started.

---

Thank you again to everyone who has contributed to this project. Your support and efforts are greatly appreciated!
