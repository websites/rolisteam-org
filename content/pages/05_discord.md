Title: Roll on discord
Date: 2017-06-11 10:20
slug: discord
status: hidden
lang: en

We developed a Discord bot to offer the power of DiceParser into the discord world.
It is really easy to use.   

![images]({static}/images/diceParserOnDiscord.png)

## How to install

Just follow this link  
[https://discord.com/api/oauth2/authorize?client_id=279722369260453888&permissions=277025429504&scope=applications.commands%20bot](https://discord.com/api/oauth2/authorize?client_id=279722369260453888&permissions=277025429504&scope=applications.commands%20bot)  

The link will prompt you to authorize the bot on a server. Once the bot's authorized, you'll see it in the Member List. In a public channel just type any of the commands outlined below and the bot will answer with a dice roll.

## Run Command

To run command, you have to send message with `/r` as first character.  
Example: this is how you can roll two 6-slided dice.

![gif]({static}/images/help/roll_dice.gif)

## The commands don't show up!

If the commands don't appear in the discord panel when typing `/`.
You should try to kick out **DiceParser** from your server and to invite again from the link above. 
If the issue is still here after that check the permission of the bot in your server settings : `Server Settings > Integration > bots and Apps > DiceParser`

## Links

Complete Documentation about [Dice Commands](https://invent.kde.org/kde/rolisteam-diceparser/-/blob/master/HelpMe.md)  
[Project Page](https://invent.kde.org/kde/rolisteam-diceparser)  


## Get the perfix back ?

The change from `!` to `/r` is due to a policy change about bots from discord. There is a way to get the permission to use former prefix system. Discord should allow it. We are waiting.