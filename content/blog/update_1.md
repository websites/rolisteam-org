date: 2022-07-03 12:00
modified: 2022-07-03 19:20
category: news
slug: update1
lang: en
authors: Rolisteam
summary: Update about development
title: June 2022 - Monthly update #1


I would like to communicate around rolisteam to explain and to describe what we are doing in the project.  
As this is the first update, it will be a bit long.
As you may see or know, Rolisteam is part of KDE project, now. Some changes have been required to reach the KDE standards.


# 1. Build system and git structure

We changed the  from **qmake**  to **cmake**. The dev branch is currently working with **cmake**.
This should allow a better management for installer creation. 

Rolisteam used to have many git submodules. It was a way to make easier the reuse of components between Rolisteam and rcse. But it makes difficult any attend to take a part in rolisteam's development. Now, all of the submodules have been added to the main rolisteam git repository. Cmake's features are currently helping a lot to manage that easily.
  
  We merged all of these repository into the main one:
  
  * RCSE : the rolisteam charactersheet editor
  * PLUGINS : the GM tools: name generator, unit convertor…
  * COMMON: many classes to manage some parts of the application (log…)
  * CHARACTERSHEET: shared classes required in both application (rcse and rolisteam) for displaying character sheet.
  
Only diceparser (the component to roll dice) is still a submodule. Other projects can use it.

## Before
![image](../images/updates/001/before_git_organisation.png)

## After
![image](../images/updates/001/after_git_organisation.png)

The complexity is gathered inside the same git repository.


# 2. Port to Qt6

Qt is the crossplatform framework which helps developers to make applications.
Writing a software is like making a building. There are a lot of type of building: skycrapers, house, nuclear plant or waterdam.
You need to put together all the pieces in a particular way. Whatever the kind of construction work you are doing, you will propably need some wires, walls, windows, pipes and also some tools (drills, cranes…). 
Qt provides all of that: Windows, button, panel, network componant, multimedia etc.
Qt released its version 6 few months ago. We had to wait until the release of Qt6.3 to be sure that rolisteam can be ported to Qt6. 
It has been done and it works. 


# 3. Architecture change

The key rule in software design is to split the front (what the user see) and the back (what the application do).
This rule was not really followed in rolistik and in rolisteam. But now, we insure that by creating some internal libraries. 
We can split the core functions and the view into two components. Of course, the view components will use some core functions but the core function won't use any view components. 
  
  The goal is too make easier any view changes. As the backend will be the same. 
  It allows a better control over what it going on and that makes objects simpler.   
  Moreover, it is easier to write some automatic test for the backend.
  
  In Rolisteam 1.9.X, most of the elements have the responsability to display something on the screen, manages all user actions, and send message through network to make sure all players are seeing the same thing.  
  We split that into 3 elements: The view, the controller and the updater.
  
  * **View**: The role is obvious it must display the element on screen and interact with the user.
  * **Controller**: stores the data, and change those data according to users action (local user or remote)
  * **Updater**: sends off messages (if local player is the game master) to notify all the other player that the state of the controller has changed. 
  

# 4. Instant messaging in QML

The instant messaging has been redesign completely. The look has changed but the inside as well.  
A video is probably better than a long description so:

![animated gif](../images/updates/001/instantmsg.gif)

Incomplete list of features:

* Sound notification
* All conversations in the same windows (by default)
* Detach conversation (put it in another windows)
* Split the view (show several at the same time in the same window)
* New look (display avatar of people, time and nickname)
* Support of UTF-8 emoji
* Dark theme
* Sound notification

The look is not done yet. We are still thinking about it. Let us know if you have idea. 

# 5. Simplify Network API

The network API has been simplified. We renamed some components for a better understanding.
We also implement a UPnP client to make the port forwarding automatically, without any configuration from the user.
It should help a lot of people.

# 6. Code coverage

The code coverage is a metric which tells how tested the code is.
When you run the automatic tests, at the end you have a number saying, 60% of the source code has been visited/used/executed.

It is very important in order to prevent issue. So we decided to implement a solution to generate this metric. We use gcov.  
It helps to know if we have enough test or not.

The code coverage of DiceParser (the dice system in rolisteam) was originaly at 66% and after some improvements (adding new tests mainly) we reach 90%.

This metric for rolisteam code is really low: only 11% at first. We implement a generic test and we reach 26%.

![code coverage](../images/updates/001/codecoverage.png)

You can click on each directory to get details about which file or code line have a low score and why. Really easy to improve.



# 7. Current work

As we change a lot of things given the new network API and the new architecture, we are currently working on making all media working again.

* Image [done]
* PDF improved [done]
* Note [done]
* Vectorial map [in progress]
* Shared Note 
* Charactersheet
