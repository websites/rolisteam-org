date: 2022-08-31 12:00
modified: 2022-08-31 19:20
category: news
slug: update3
lang: en
authors: Rolisteam
summary: August updates about rolisteam development
title: Rolisteam Monthly update #3 - August 2022


Third issue about the progress on rolisteam. 





# 1. RCSE

**R**olisteam **C**haracter**S**heet **E**ditor

Short introduction, the RCSE allows you to create charactersheet for any TTRPG.
It is based on a visual editor to draw fields directly upon an image of the charactersheet.
The editor part is using: `QGraphicsView/QGraphicsScene` and a table view to edit each field.
Then the final result can be generated to get the sheet in QML. 

User can edit the generated code directly. After merging almost all repo into the main one (see [Update of june]({filename}/blog/update_1.md)), RCSE was really unstable and some features weren't working anymore.


Some changes in details:

## The UI Improvements

**RCSE in v1.9.0**
![RCSE_v1.9]({static}/images/updates/003/rcse_v1_9.png)


**Improved UI of RCSE**
![RCSE_v1.8]({static}/images/updates/003/rcse_now.png)


## New QML generation

In the earlier version of **RCSE**, there were 2 ways to generate the QML. 
The first option was to generate a qml item that adapt itself to the size of the windows. The second option was to create an item inside a flickview at the fixed scale value and if the window was to small to display the whole sheet. Users can scroll up/down/right/left to see the proper section of the sheet.

We remove those two options and now, by default the sheet adapts itself to the windows and there are actions to change the zoom factor. So user can decide the size of the sheet. Of course, scrolling is still possible.
We basically merged both options into one.


## Add support for formula into the field model

Some users complain about the fact that formula didn't work in the field model.   
There were working only in the character's table.
It is fixed now, and formula are duplicated into character (at character creation).

![RCSE_v1.8]({static}/images/updates/003/formula.gif)



## Changes:

* Change architecture to have a MainController
* Exposing C++ API from a singleton instead of rootProperty.
* Fix image models
* Add right click feature to control zoom
* Removal
* UI Improvements
* Save loglevel and restore it on the next start
* Add slider item for sheet
* Add hidden field for intermediate computation (feature requirement from user).
* and others…



# Vectorial Map

This month, we spent time over vectorial map as well. 
The main changes that we introduce are about:

## The small view

It allows user to see an overview of map. It is also possible to see the visible section of the map.
It is really helpful to understand the relus.

![maps]({static}/images/updates/003/vmap.gif)


## GameMaster Layer

As you may know map already had Ground, Object and Character layer. 
User wrote a ticket asking for an additionnal layer to put elements hidden from the players but visible for GM. 
To mark that items belong to the GameMaster layer, they become half transparent.


## Features:

1. Highlighter is using user color, now.
2. Improvements about zooming and scrolling over the scene.
3. Improve changing z order of map items (still in development)



# 5. Work for September

* Keep working on Vectorial map
* Network part for vmap
