import subprocess
import webbrowser
import os
from os.path import exists
PELICAN_PATH = os.path.expanduser("~/.local/bin/pelican")
pelican_check = exists(PELICAN_PATH)
if pelican_check == True:
    if os.name=="nt":
        print("NT OS DETECTED so not working! Pelican needs Pelican-toc libraries and this is not available at the moment")
    else:
        def build_site():
            subprocess.run([PELICAN_PATH, '-lr'])

        def open_browser():
                webbrowser.open("http://localhost:8000")

        if __name__ == "__main__":
                build_site()
                open_browser()
else:
    print("Pelican Installation in progress.")
    os.system("pip3 install -r requirement.txt")
    print("Starting Server restart !")
    exit()